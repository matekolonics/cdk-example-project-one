# CDK Example Project - Simple
This is a simple webapp infrastructure implemented in AWS CDK. The project also contains a CloudFormation template (`cloudformation.yml`). \
Resources defined in this stack:
- EC2 instance (server for running code)
- RDS instance (database)
- S3 bucket (for storing files)

You can find the more advanced version [here](https://gitlab.com/matekolonics/cdk-example-project-two).
# Working with CDK
## Creating a new CDK Project
### 1. Install CDK
```bash
npm i -g aws-cdk
```
### 2. Bootstrap CDK environment
```bash
cdk bootstrap aws://ACCOUNT-NUMBER/REGION
```
### 3. Create project directory
```bash
mkdir cdk-example-project
cd cdk-example-project
```
### 4. Initialize CDK app
```bash
cdk init app --language typescript
```
## Generating a CloudFormation template
Running the following command will generate the template and output it in the terminal:
```bash
cdk synth
```
\
 If you would like to save it to a file instead, run the following command:
 ```bash
 cdk synth > cloudformation.yaml
 ```
 \
 Now you can deploy this template file using CloudFormation.
 ## Deploying using CDK CLI
 You can also deploy your CDK project without CloudFormation (`cdk synth` is not needed if you choose this method).
 To do so, run the following command:
 ```bash
 cdk deploy
 ``` 
