import * as cdk from 'aws-cdk-lib';
import {aws_ec2, aws_rds, aws_s3, RemovalPolicy, SecretValue} from 'aws-cdk-lib';
import {Construct} from 'constructs';
import {AmazonLinuxCpuType, InstanceClass, InstanceSize, InstanceType, Peer, Port} from "aws-cdk-lib/aws-ec2";
import {Credentials, DatabaseInstanceEngine} from "aws-cdk-lib/aws-rds";

export class CdkExampleSimpleStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const vpc = new aws_ec2.Vpc(this, 'ExampleVPC', {});

    const securityGroup = new aws_ec2.SecurityGroup(this, 'ExampleSG', {
      vpc
    });
    securityGroup.addIngressRule(Peer.ipv4('0.0.0.0/0'), Port.allTraffic());

    const ami = aws_ec2.MachineImage.latestAmazonLinux2023({
      cpuType: AmazonLinuxCpuType.ARM_64,
    });

    const ec2Instance = new aws_ec2.Instance(this, id, {
      vpc,
      instanceType: InstanceType.of(
          InstanceClass.T4G,
          InstanceSize.SMALL,
      ),
      securityGroup,
      machineImage: ami,
    });

    const rds = new aws_rds.DatabaseInstance(this, 'ExampleDB', {
      vpc,
      engine: DatabaseInstanceEngine.POSTGRES,
      instanceType: InstanceType.of(
          InstanceClass.T4G,
          InstanceSize.MICRO,
      ),
      databaseName: 'example',
      credentials: Credentials.fromPassword('root', SecretValue.unsafePlainText('Test1234')),
      removalPolicy: RemovalPolicy.DESTROY,
    });

    const s3Bucket = new aws_s3.Bucket(this, 'ExampleBucket', {});
  }
}
